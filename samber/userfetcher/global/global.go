package global

import (
	"github.com/samber/do"
	"gitlab.com/albandewilde/tdi/samber/userfetcher"
)

func init() {
	do.Provide(
		nil,
		func(i *do.Injector) (*userfetcher.UserFetch, error) {
			return userfetcher.NewUserFetch("example.com", "key"), nil
		},
	)
}
