package samber

import (
	"testing"

	"github.com/samber/do"
	flf "gitlab.com/albandewilde/tdi/samber/locationfetcher/fake"
	fuf "gitlab.com/albandewilde/tdi/samber/userfetcher/fake"
)

func TestOurApp(t *testing.T) {
	app := NewApp(
		do.MustInvoke[*fuf.FakeUserFetch](nil),
		do.MustInvoke[*flf.FakeLocationFetch](nil),
	)

	app.DoStuff()
}
